/*==================== toggle icon navbar ====================*/
let menuIcon = document.querySelector('#Menu-Icon');
let navbar = document.querySelector('nav');
menuIcon.onclick = () => 
{
    menuIcon.classList.toggle('bx-x');
    navbar.classList.toggle('Active');
};
/*==================== scroll sections active link ====================*/
let sections = document.querySelectorAll('section');
let navLinks = document.querySelectorAll('header nav ul li a');
window.onscroll = () => 
{
    sections.forEach(sec => 
    {
        let top = window.scrollY;
        let offset = sec.offsetTop - 150;
        let height = sec.offsetHeight;
        let id = sec.getAttribute('id');
        if(top >= offset && top < offset + height)
        {
            navLinks.forEach(links => 
            {
                links.classList.remove('Active');
                document.querySelector('header nav ul li a[href*=' + id + ']').classList.add('Active');
            });
        };
    });
    /*==================== sticky navbar ====================*/
    let header = document.querySelector('header');
    header.classList.toggle('sticky', window.scrollY > 100);
    /*==================== remove toggle icon and navbar when click navbar link (scroll) ====================*/
    menuIcon.classList.remove('bx-x');
    navbar.classList.remove('Active');
};
/*==================== scroll reveal ====================*/
ScrollReveal(
    { 
        /*reset: true,*/
        distance: '80px',
        duration: 2000, 
        delay: 200
    }
);
ScrollReveal().reveal('.Home-Content, .Heading', { origin: 'top' });
ScrollReveal().reveal('.Home-Img, .Services-Container, .Portafolio-Box, .Contact form', { origin: 'bottom' });
ScrollReveal().reveal('.Home-Content h1, .About-Img', { origin: 'left' });
ScrollReveal().reveal('.Home-Content p, .About-Content', { origin: 'right' });
/*==================== typed js ====================*/
const typed = new Typed('.Multiple-Text', 
{
    strings: ['Web Developer', 'Blogger', 'YouTuber', 'Entrepreneur'],
    typeSpeed: 100, 
    backSpeed: 100, 
    backDelay: 1000, 
    loop: true
}); 